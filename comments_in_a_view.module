<?php

/**
 * @file
 * Drupal hooks and helper code to implement Comments In A View functionality.
 *
 * Alters various Drupal forms and menus to replace the standard comment listing
 * with a View.
 */

/**
 * Implementation of hook_menu_alter().
 */
function comments_in_a_view_menu_alter(&$items) {
  $items['node/%node']['page callback'] = 'comments_in_a_view_page_view';
}

/**
 * Implementation of hook_form_alter().
 */
function comments_in_a_view_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'node_type_form' && isset($form['identity']['type'])) {
    $options = array('' => t('None: Use Drupal defaults'));

    $views = views_get_all_views();
    if (is_array($views)) {
      foreach ($views as $key => $view) {
        if ($view->base_table == 'comments') {
          $options[$key] = $view->name;
        }
      }
    }

    $form['comment']['comment']['#weight'] = -10;
    $form['comment']['comments_in_a_view'] = array(
      '#type' => 'select',
      '#title' => t('Use a View to display comments'),
      '#options' => $options,
      '#default_value' => variable_get('comments_in_a_view_'. $form['#node_type']->type, ''),
      '#weight' => -1,
    );
  }
}

/**
 * Menu callback; takes the place of Node module's default function.
 */
function comments_in_a_view_page_view($node, $cid = NULL) {
  drupal_set_title(check_plain($node->title));
  return comments_in_a_view_show($node, $cid);
}

/**
 * Generate a page displaying a single node, along with its comments.
 */
function comments_in_a_view_show($node, $cid, $message = FALSE) {
  if ($message) {
    drupal_set_title(t('Revision of %title from %date', array('%title' => $node->title, '%date' => format_date($node->revision_timestamp))));
  }
  $output = node_view($node, FALSE, TRUE);
  if ($node->comment) {
    if (variable_get('comments_in_a_view_'. $node->type, 'comments_in_a_view')) {
      $output .= comments_in_a_view_render($node, $cid);
    }
    elseif (function_exists('comment_render')) {
      $output .= comment_render($node, $cid);
    }
  }

  // Update the history table, stating that this user viewed this node.
  node_tag_new($node->nid);

  return $output;
}

/**
 * Render the comments for a node, along with the comment form.
 */
function comments_in_a_view_render($node, $cid = 0) {
  global $user;
  if (user_access('access comments')) {
    // Pre-process variables.
    $nid = $node->nid;
    if (empty($nid)) {
      $nid = 0;
    }
    
    if (is_numeric($cid)) {
      // Single comment view.
      if ($comment = node_load($cid)) {
        $output = theme('node', $comment, TRUE, TRUE);
      }
    }
    else {
      $view_name = variable_get('comments_in_a_view_'. $node->type, 'comments_in_a_view');
      if ($view_name) {
        $output = views_embed_view($view_name, 'default', array($nid));
        $view = views_get_view($view_name);
        $view->set_display('default');
        if ($view->access('default')) {
          $view->current_account = $user;
          $output = $view->execute_display('default', array($node->nid, $cid));
        }
      }
    }

    // If enabled, show new comment form if it's not already being displayed.
    $reply = arg(0) == 'comment' && arg(1) == 'reply';
    if (user_access('post comments') && node_comment_mode($nid) == COMMENT_NODE_READ_WRITE && (variable_get('comment_form_location_'. $node->type, COMMENT_FORM_SEPARATE_PAGE) == COMMENT_FORM_BELOW) && !$reply) {
      $output .= comment_form_box(array('nid' => $nid), t('Post new comment'));
    }

    if ($output) {
      $output = theme('comment_wrapper', $output, $node);
    }
  }

  return $output;
}

/**
 *  Implementation of hook_views_api().
 */
function comments_in_a_view_views_api() {
  return array(
    'api' => 2.0,
  );
}